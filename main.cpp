#include <iostream>
#include "ctime"
#include "matrixOp.h"

using namespace std;

const int lowerBound = -11;
const int upperBound = 9;

int main() {
	srand(0);
	int matrix[MATRIX_SIZE][MATRIX_SIZE]{};

	RandomiseMatrix(matrix, lowerBound, upperBound);

	bool running = true;
	int replacement[MATRIX_SIZE]{};
	while (running) {
		cout << "1) Print the matrix\n"
			 << "2) Fill the matrix with new random values\n"
			 << "3) Rearrange the numbers in row according to the rule: negative positive zeroes\n"
			 << "4) Replace all zero filled rows with a defined vector\n"
			 << "5) Calculate the sum of negative elements in the black area\n"
			 << "0) Exit the program\n"
			 << "Please enter a number between 0 and 5" << endl;
		int choice = 0;
		cin >> choice;
		switch (choice) {
			case 0:
				running = false;
				break;
			case 1:
				PrintMatrix(matrix);
				system("pause");
				break;
			case 2:
				srand(time(nullptr));
				RandomiseMatrix(matrix, lowerBound, upperBound);
				break;
			case 3:
				RearrangeMatrix(matrix);
				break;
			case 4:
				cout << "Enter the replacement vector: ";
				for (int &i : replacement) {
					cin >> i;
				}
				ReplaceZeroes(matrix, replacement);
				break;
			case 5:
				cout << "Sum of all negative numbers below the main diagonal: "
					 << CalculateSumOfNegativesInBlackArea(matrix) << endl;
				system("pause");
				break;
			default:
				cout << "Please enter a valid number" << endl;
		}
	}
	return 0;
}
