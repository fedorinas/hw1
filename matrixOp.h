#ifndef HW1_MATRIXOP_H
#define HW1_MATRIXOP_H
#include <iostream>

const int MATRIX_SIZE=8;

void RandomiseMatrix(int [][MATRIX_SIZE], int, int);

void PrintMatrix(int [][MATRIX_SIZE]);

void RearrangeMatrix(int[][MATRIX_SIZE]);

void ReplaceZeroes(int[][MATRIX_SIZE], const int[MATRIX_SIZE]);

int CalculateSumOfNegativesInBlackArea(int[][MATRIX_SIZE]);

#endif //HW1_MATRIXOP_H
