cmake_minimum_required(VERSION 3.17)
project(HW1)

set(CMAKE_CXX_STANDARD 20)

add_executable(HW1 main.cpp matrixOp.h matrixOp.cpp)