#include "matrixOp.h"

void RandomiseMatrix(int matrix[][MATRIX_SIZE], int lowerBound, int upperBound) {
	for (int i = 0; i < MATRIX_SIZE; i++) {
		for (int j = 0; j < MATRIX_SIZE; j++) {
			matrix[i][j] = rand() % (upperBound - lowerBound) + lowerBound;
		}
	}
}

void PrintMatrix(int matrix[][MATRIX_SIZE]) {
	for (int i = 0; i < MATRIX_SIZE; i++) {
		for (int j = 0; j < MATRIX_SIZE; j++) {
			std::cout << '\t' << matrix[i][j];
		}
		std::cout << std::endl;
	}
}

void RearrangeMatrix(int matrix[][MATRIX_SIZE]) {
	for (int i = 0; i < MATRIX_SIZE; i++) {
		for (int j = 0; j < MATRIX_SIZE-1; j++) {
			for (int k = 0; k < MATRIX_SIZE-1 - j; k++) {
				if (matrix[i][k] > matrix[i][k + 1]) {
					std::swap(matrix[i][k], matrix[i][k + 1]);
				}
			}
		}
	}

	for (int i = 0; i < MATRIX_SIZE; i++) {
		int k = MATRIX_SIZE-1;
		for (int j = 0; j < k; j++) {
			if (matrix[i][j] == 0) {
				while (matrix[i][k] == 0) {
					k--;
				}
				std::swap(matrix[i][k], matrix[i][j]);
			}
		}
	}
}

void ReplaceZeroes(int matrix[][MATRIX_SIZE], const int replacement[MATRIX_SIZE]) {
	for (int i = 0; i < MATRIX_SIZE; ++i) {
		bool isZeroes = true;
		for (int j = 0; j < MATRIX_SIZE; ++j) {
			if (matrix[i][j] != 0) {
				isZeroes = false;
			}
		}
		if (isZeroes) {
			for (int j = 0; j < MATRIX_SIZE; ++j) {
				matrix[i][j] = replacement[j];
			}
		}
	}
}

int CalculateSumOfNegativesInBlackArea(int matrix[][MATRIX_SIZE]) {
	int sum = 0;
	for (int i = 0; i < MATRIX_SIZE; ++i) {
		for (int j = 0; j < i + 1; ++j) {
			if (matrix[i][j] < 0) {
				sum += matrix[i][j];
			}
		}
	}
	return sum;
}